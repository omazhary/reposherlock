.PHONY: env-setup dev-install clean-dev clean-package help publish

.DEFAULT_GOAl = help

help:
	@echo "--------------------------------------------- Usage ---------------------------------------------"
	@echo "> env-setup: Sets up the basic development environment using virtualenv and installs dependencies"
	@echo "> dev-install: Installs the package in development mode for local runs and testing"
	@echo "> clean-dev: Removes the virtual environment folder"
	@echo "> clean-package: Removes the packaged version of the module in the egg-info file"
	@echo "-------------------------------------------------------------------------------------------------"

env-setup:
	virtualenv -p python3 venv
	venv/bin/pip3 install -r requirements.txt

dev-install:
	venv/bin/pip3 install -e .

clean-dev:
	rm -rf venv

clean-package:
	rm -rf *.egg-info
	rm -rf build
	rm -rf dist

publish:
	python setup.py sdist bdist_wheel
	twine check dist/*
	twine upload dist/*
